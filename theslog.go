package theslog

import (
	"log/slog"
	"os"
	"time"

	"gitlab.com/greyxor/slogor"
)

func Init() {
	logger := slog.New(slogor.NewHandler(os.Stderr, &slogor.Options{
		Level:      slog.LevelDebug,
		TimeFormat: time.DateTime,
		ShowSource: true,
	}))

	slog.SetDefault(logger)
}

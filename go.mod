module gitlab.com/avoronkov/theslog/v2

go 1.21.4

require gitlab.com/greyxor/slogor v1.2.0

require golang.org/x/sys v0.14.0 // indirect
